import ballerinax/kafka;
import ballerina/io;
import ballerina/log;
import ballerina/mime;

// Constants to store client credentials
final string CLIENT_USERNAME = "PRODUCER";
final string CLIENT_PASSWORD = "PRODUCER";

// Kafka producer configurations
kafka:ProducerConfiguration producerConfigs = {
    // Here we created a producer configs with optional parameters client id - used for broker side logging.
    clientId:"file-storage-producer",
    // number of acknowledgments for request complete
    acks: "all",
     // number of retries if record send fails.
    metricsNumSamples: 4,
    schemaRegistryUrl: "http://localhost:9092"
}; 
 

kafka:Producer kafkaProducer = check new (kafka:DEFAULT_URL);

public function main() returns error? {
    string message = "Testing Message transmision in Ballerina testing fifth and last";
    // Sends the message to the Kafka topic.
    check kafkaProducer->send({
                                topic: "kafkatest",
                                value: message.toBytes() });
    // Flushes the sent messages.
    check kafkaProducer->'flush();
}
// HTTP service endpoint
listener http:Listener httpListener = new(9091);

@http:ServiceConfig { basePath: "/myfile" }
service clientService on httpListener {


    @http:ResourceConfig { methods: ["POST"], consumes: ["application/json"], produces: ["application/json"] }
    resource function writeToFile(http:Caller caller, http:Request request, json content, string path) {
        http:Response response = new;

       io:println(" writting to file operation :");
       io:ByteChannel byteChannel = io:openFile(path, io:WRITE);
       io:CharacterChannel ch = new io:CharacterChannel(byteChannel, "UTF8")

       match ch.writeJson(content) {
           error err => {
               close(ch);
               throw err;
           }
            () => {
                close(ch);
                io:println("Content written successfully");
            }
        }
    
        // Initialise the file path
        string filePath = "./dsa_version02/files.txt";
        
        // reads the file content as a byte array
        byte[] bytes = check io:fileReadBytes(filePath);
        
        // Hashing bytes value using the MD5 hashing algorithm
        // Printing the value using the Hex encoding
        byte[] output = crypto:hashMd5(bytes);
        io:println("Hex encoded hash with MD5: " + output.toBase16());

        // writes file to the given destination
        check io:fileWriteBytes(fileCopyPath1, bytes);
        io:println("Successfully copied the file as a byte array.");

        // overridde the default file Size by the value of 30 MB
        stream<io:Block, io:Error?> blockStream = check
        io:fileReadBlocksAsStream(filePath, 31457280);

        // writting content to the given destination using the given stream
        check io:fileWriteBlocksFromStream(fileCopyPath2, blockStream);
        io:println("Successfully copied the file as a stream.");

        json|error reqPayload = request.getJsonPayload();

        if (reqPayload is error) {
            response.statusCode = 400;
            response.setJsonPayload({ "Message": "Invalid payload - Not a valid JSON payload" });
            var result = caller->respond(response);
        } else {
            json|error username = reqPayload.Username;
            json|error password = reqPayload.Password;
            json|error filePath = reqPayload.File;
            json|error fileSize = reqPayload.filefileSize;

            // If payload parsing fails, it send a "Bad Request" message as the response
            if (username is error || password is error || filePath is error || fileSize is error) {
                response.statusCode = 400;
                response.setJsonPayload({ "Message": "Bad Request: Invalid payload" });
                var responseResult = caller->respond(response);
            } else {
		// Convert the file filefileSize value to byte value
		var result = byte:fromString(fileSize.toString());
		if (result is error) {
		    response.statusCode = 400;
		    response.setJsonPayload({ "Message": " File too large than 30 MB" });
		    var responseResult = caller->respond(response);
		} else {
		    newFileSize = result;
		}

		// If the credentials does not match with the client credentials,
		// send an "Access Forbidden" response message
		if (username.toString() != CLIENT_USERNAME || password.toString() != CLIENT_PASSWORD) {
		    response.statusCode = 403;
		    response.setJsonPayload({ "Message": "Access Forbidden" });
		    var responseResult = caller->respond(response);
		}

		// Construct and serialise the message to be published to the Kafka topic
		json fileWriteInfo = { "File": filePath, "fileSize": newFileSize };
		byte[] serialisedMsg = fileWriteInfo.toString().toByteArray("UTF-8");

		// Produce the message and publish it to the Kafka topic
		var sendResult = kafkaProducer->send(serialisedMsg, "DSA test 1", partition = 0);
		// Send internal server error if the sending has failed
		if (sendResult is error) {
		    response.statusCode = 500;
		    response.setJsonPayload({ "Message": "Kafka producer failed to send data" });
		    var responseResult = caller->respond(response);
		}
		// Send a success status to the client request
		response.setJsonPayload({ "Status": "Success" });
		var responseResult = caller->respond(response);
	    }
        }
    }
    public type storageService record{
    string fileName;
    string filePath;
    byte[] fileSize;
    };
    string schema = "{\"type\" : \"record\"," +
                  "\"namespace\" : \"Thomas\"," +
                  "\"fileName\" : \"file\"," +
                  "\"fields\" : [" + 
                    "{ \"fileName\" : \"fileName\", \"type\" : \"string\" }," +
                    "{ \"fileName\" : \"filePath\", \"type\" : \"string\" }," +
                    "{ \"fileName\" : \"fileSize\", \"type\" : \" byte\" }," +
                    "]}";
                    
    @http:ResourceConfig { methods: ["GET"], consumes: ["application/json"], produces: ["application/json"] }
    resource function readFile(http:Caller caller, http:Request request, string filePath) returns json {
        http:Response response = new;                
    // Create a byte channel from the given file path
    io:ByteChannel byteChannel = io:openFile(filePath, io:READ);
    // Derive the character channel from the byte channel
    io:CharacterChannel ch = new io:CharacterChannel(byteChannel, "UTF8");
    // This is how json content is read from the character channel
    match ch.readJson() {
        json result => {
            close(ch);
            return result;
        }
        error err => {
            close(ch);
            throw err;
        }
    }

    function handleFileContent(mime:Entity fileContent) returns string {
        mime:MediaType mediaType = check mime:getMediaType(fileContent.getContentType());
        string baseType = mediaType.getBaseType();
        byte[] fileSize = check fileContent.getByteArray();
        string base64 = mime:byteArrayToString(mime:base64EncodeByteArray(fileSize), "UTF8");
        return base64;
    }
    function insertToMyFileService(string fileName, string filePath, byte[] fileSize) returns boolean {
    log:printInfo("Inserting file to myFileService: ");
    boolean success = false;

    transaction with retries = 4, oncommit = onCommitFunction, onabort = onAbortFunction {
        var result = myFileServiceEP->write(REQUEST_TO_INSERT_FILE_TO_STORAGESERVICE, fileName, filePath);

        match result {
            int c => {
                if (c < 0) {
                    log:printError("Unable to insert File into myfileService record:");
                } else {
                    log:printInfo("Successful! Inserted Into myFileService record: ");
                    success = true;
                }
            }
            error err => {
                log:printError(err.message);
            }
        }
    } onretry {
        io:println("Retrying transaction");
    }
    return success;
    }

    function setErrorResponse(http:Response response, error e, int statusCode) {
        response.statusCode = statusCode;
        response.reasonPhrase = e.message;
        response.setJsonPayload(check <json>e);
    }
    
    function onCommitFunction(string transactionId) {
        io:println("Transaction: " + transactionId + " committed");
    }

    function onAbortFunction(string transactionId) {
        io:println("Transaction: " + transactionId + " aborted");
    }

    function main (string...args) {
        string filePath = "./C:/Users/Thomas/Desktop/dsa_version02";
        Json fileData = {"fileName":"",
                   "fileSize":""}
    };
    int count = 0;
    json []allSt;
    while (count <= 1){
    json temp;
    temp.fileName =
    ){

    }
    fileData.fileName = io:readln("Enter your file Name");
    fileData.fileSize = io:readln("Enter your file size");
    
    io:println("Preparing to write json file");
    write(fileData, filePath);

    io:println("Preparing to read the content written");
    json content = read(filePath);
    io:println(content);

    kafka:StorageRecord storageRecord = {
        schemaString: schema,
        dataRecord: myFile
    };

    var result = producer->send(storageRecord, "add-file-with-fileName");
    if (result is kafka:ProducerError) {
        io:println(result);
    } else {
        io:println(" file Successfuly sent");
    }
    }
}
